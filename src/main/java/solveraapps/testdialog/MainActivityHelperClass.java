package solveraapps.testdialog;


import android.content.Context;

public class MainActivityHelperClass {

    public static int getResourceIdArray(Context context , String string){
        int arrayName_ID = context.getResources().getIdentifier(string , "array",context.getPackageName());
        return arrayName_ID;
    }


    public static String getStringResourceByName(Context context, String string) {
        String sReturn="";
        try{
            int resId = context.getResources().getIdentifier(string, "string", context.getPackageName());
            sReturn=context.getString(resId);

        }catch(Exception e){
            sReturn="String not found for'" + string + "'";
        }
        return sReturn;
    }



}
