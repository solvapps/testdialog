package solveraapps.testdialog.dialogs;

import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import solveraapps.testdialog.MainActivity;
import solveraapps.testdialog.R;
import solveraapps.testdialog.dataclasses.OptionHandler;

/**
 * Created by andreas on 02.12.2017.
 */

public class MapOptionDialog {

    public MapOptionDialog(MainActivity activity, OptionHandler optionHandler){
        this.activity = activity;
        this.optionHandler = optionHandler;
    }

    MainActivity activity;
    OptionHandler optionHandler;



    public void showDialog(){

//        optionHandler = new MapOptions();

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.mapoptiondialog,null);

        Button cancelMapOptions = (Button) mView.findViewById(R.id.cancelMapOptions);
        Button saveMapOptions = (Button) mView.findViewById(R.id.saveMapOptions);
        final Switch switchShowEvents = (Switch) mView.findViewById(R.id.showEvents);
        switchShowEvents.setChecked(optionHandler.getMapOptions().isShowEvents());

        mBuilder.setTitle("Map Options");

        mBuilder.setView(mView);

        final AlertDialog mDialog = mBuilder.create();
        mDialog.show();
        cancelMapOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        saveMapOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionHandler.getMapOptions().setShowEvents(switchShowEvents.isChecked());
                optionHandler.saveMapOptions();
                mDialog.dismiss();
            }
        });


    }



}
