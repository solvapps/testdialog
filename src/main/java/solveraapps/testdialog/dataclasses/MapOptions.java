package solveraapps.testdialog.dataclasses;

/**
 * Created by andreas on 01.12.2017.
 */

public class MapOptions {

    boolean showEvents = true;
    boolean showGeo = true;
    boolean showCities = true;
    boolean showTexts = true;
    boolean showRoutes = true;

    int amoutEvents = 15;


    public boolean isShowEvents() {
        return showEvents;
    }

    public void setShowEvents(boolean showEvents) {
        this.showEvents = showEvents;
    }

    public boolean isShowGeo() {
        return showGeo;
    }

    public void setShowGeo(boolean showGeo) {
        this.showGeo = showGeo;
    }

    public boolean isShowCities() {
        return showCities;
    }

    public void setShowCities(boolean showCities) {
        this.showCities = showCities;
    }

    public boolean isShowTexts() {
        return showTexts;
    }

    public void setShowTexts(boolean showTexts) {
        this.showTexts = showTexts;
    }

    public boolean isShowRoutes() {
        return showRoutes;
    }

    public void setShowRoutes(boolean showRoutes) {
        this.showRoutes = showRoutes;
    }

    public int getAmoutEvents() {
        return amoutEvents;
    }

    public void setAmoutEvents(int amoutEvents) {
        this.amoutEvents = amoutEvents;
    }

    @Override
    public String toString() {
        return "MapOptions{" +
                "showEvents=" + showEvents +
                ", showGeo=" + showGeo +
                ", showCities=" + showCities +
                ", showTexts=" + showTexts +
                ", showRoutes=" + showRoutes +
                ", amoutEvents=" + amoutEvents +
                '}';
    }
}
