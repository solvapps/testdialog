package solveraapps.testdialog.dataclasses;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import solveraapps.testdialog.R;


/**
 * Created by andreas on 05.12.2017.
 */

public class TimelineOptionDialog {

    private TextView labelDefaultTextSize;
    private TextView labelTextSizeTimeline;
    private TextView labelPhaseSize;
    private TextView labelWatermark;

    private Spinner defaultTextSizeSpinner;
    private Spinner textSizeTimelineSpinner;
    private Spinner phaseSizeSpinner;

    private Switch watermarkSwitch;

    Button cancelTimelineOptions;
    Button saveTimelineOptions;

//    Activity activity;
    OptionHandler optionHandler;
//    View callingView;
    Context context;
    View mView;
    OptionDialogPostProcess optionDialogPostProcess;


    public TimelineOptionDialog(OptionDialogPostProcess optionDialogPostProcess,OptionHandler optionHandler){
//        this.activity = (Activity) callingView.getContext();
        this.optionHandler = optionHandler;
//        this.callingView = callingView;
        this.context=optionDialogPostProcess.getContext();
        this.optionDialogPostProcess = optionDialogPostProcess;

        assignResources();
        assignLabelTexts();
        setControlValues();
    }

    public void assignResources(){

        LayoutInflater inflater = LayoutInflater.from(context);

//        LayoutInflater inflater = (android.view.LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        mView = inflater.inflate(R.layout.timelineoptiondialog,null);
        labelDefaultTextSize = (TextView) mView.findViewById(R.id.labelDefaultTextSize);
        labelTextSizeTimeline = (TextView) mView.findViewById(R.id.labelTextSizeTimeline);
        labelPhaseSize = (TextView) mView.findViewById(R.id.labelPhaseSize);
        labelWatermark = (TextView) mView.findViewById(R.id.labelWatermark);

        defaultTextSizeSpinner = (Spinner) mView.findViewById(R.id.defaultTextSizeSpinner);
        textSizeTimelineSpinner = (Spinner) mView.findViewById(R.id.textSizeTimelineSpinner);
        phaseSizeSpinner = (Spinner) mView.findViewById(R.id.phaseSizeSpinner);
        watermarkSwitch = (Switch) mView.findViewById(R.id.watermarkSwitch);

        // Get Button Resources
        cancelTimelineOptions = (Button) mView.findViewById(R.id.cancelTimelineOptions);
        saveTimelineOptions = (Button) mView.findViewById(R.id.saveTimelineOptions);


    }

    public void assignLabelTexts(){

        String language = "en";

        // Hole Label Texte von String.xml
        String textsizeintextview = solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context, "textsizeintextview_" + language);
        String textsizeintimeline = solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context, "textsizeintimeline_" + language);
        String phasesizeintimeline = solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context, "phasesizeintimeline_" + language);
        String optionwatermark = solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context, "optionwatermark_" + language);

        labelDefaultTextSize.setText(textsizeintextview);
        labelTextSizeTimeline.setText(textsizeintimeline);
        labelPhaseSize.setText(phasesizeintimeline);
        labelWatermark.setText(optionwatermark);

        // Spinner Werte setzen
        int size5ID =  solveraapps.testdialog.MainActivityHelperClass.getResourceIdArray(context,"size5array_" + language);
        int size3ID =  solveraapps.testdialog.MainActivityHelperClass.getResourceIdArray(context,"size3array_" + language);

        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(context, size5ID, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(context, size3ID, android.R.layout.simple_spinner_item);
        adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        defaultTextSizeSpinner.setAdapter(adapter5);
        phaseSizeSpinner.setAdapter(adapter5);
        textSizeTimelineSpinner.setAdapter(adapter3);

        String saveMapOptionsText=solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context, "saveMapOptions_" + language);
        String cancelMapOptionsText=solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context, "cancelMapOptions_" + language);

        // Setze Texte anhand Sprache
        cancelTimelineOptions.setText(cancelMapOptionsText);
        saveTimelineOptions.setText(saveMapOptionsText);

    }
    public void setControlValues(){
//        // Allgemeine Optionen
//        watermarkSwitch.setChecked(optionHandler.isShowWaterMark());
//        defaultTextSizeSpinner.setSelection(optionHandler.getTextSizeTextViewer());
//        // Timeline spezifische Optionen
//        textSizeTimelineSpinner.setSelection(optionHandler.getTimelineOptions().getTextSizeTimeline());
//        phaseSizeSpinner.setSelection(optionHandler.getTimelineOptions().getPhaseSize());
    }

    public void showDialog() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        String language = "en";
        String title = solveraapps.testdialog.MainActivityHelperClass.getStringResourceByName(context,"optionstitle_" + language);
        mBuilder.setView(mView);

        final AlertDialog mDialog = mBuilder.create();
        mDialog.show();
        cancelTimelineOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        saveTimelineOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



    }

}
