package solveraapps.testdialog.dataclasses;

/**
 * Created by andreas on 02.12.2017.
 */

public interface IOptionHandler {

    public MapOptions getMapOptions();
    public void saveMapOptions();


}
