package solveraapps.testdialog.dialogs;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import solveraapps.testdialog.R;

/**
 * Created by andreas on 05.12.2017.
 */

public class MainMenuDialog {

    Button buttonExit;
    Button buttonInfo;
    Button buttonContact;
    Button buttonHelp;
    Button buttonBookmarks;
    Button buttonOptions;

    Context context;
    IMainMenuCallBack iMainMenuCallBack;
    View mView;

    public MainMenuDialog(IMainMenuCallBack iMainMenuCallBack, Context context){

        this.context = context;
        this.iMainMenuCallBack = iMainMenuCallBack;
        assignResources();
        assignTexts();
    }

    public void assignResources(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        mView = inflater.inflate(R.layout.mainmenu,null);
        buttonExit = (Button) mView.findViewById(R.id.buttonExit);
        buttonBookmarks = (Button) mView.findViewById(R.id.buttonBookmarks);
        buttonContact = (Button) mView.findViewById(R.id.buttonContact);
        buttonHelp = (Button) mView.findViewById(R.id.buttonHelp);
        buttonInfo = (Button) mView.findViewById(R.id.buttonInfo);
        buttonOptions = (Button) mView.findViewById(R.id.buttonOptions);

    }

    public void assignTexts(){

        String labelOptions= "options";
        String labelExit= "exit";
        String labelInfo= "info";
        String labelContact= "contact";
        String labelHelp= "help";
        String labelBookmark= "bookmark";

        buttonExit.setText(labelExit);
        buttonOptions.setText(labelOptions);
        buttonInfo.setText(labelInfo);
        buttonContact.setText(labelContact);
        buttonHelp.setText(labelHelp);
        buttonBookmarks.setText(labelBookmark);

    }

    public void showDialog(){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context, R.style.TransparentDialogStyle);
//        mBuilder.setTitle("Map Options");
        mBuilder.setView(mView);

        final AlertDialog mDialog = mBuilder.create();
        mDialog.show();
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainMenuCallBack.mainMenuCallBackExit();
                mDialog.dismiss();
            }
        });

        buttonOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        buttonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainMenuCallBack.mainMenuCallBackInfo();
                mDialog.dismiss();
            }
        });

        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainMenuCallBack.mainMenuCallBackContact();
                mDialog.dismiss();
            }
        });

        buttonHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainMenuCallBack.mainMenuCallBackHelp();
                mDialog.dismiss();
            }
        });
        buttonBookmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainMenuCallBack.mainMenuCallBackBookmark();
                mDialog.dismiss();
            }
        });
    }


}
