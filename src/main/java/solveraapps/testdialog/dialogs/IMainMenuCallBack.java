package solveraapps.testdialog.dialogs;


/**
 * Created by andreas on 06.12.2017.
 */

public interface IMainMenuCallBack {
    public void mainMenuCallBackInfo();
    public void mainMenuCallBackExit();
    public void mainMenuCallBackContact();
    public void mainMenuCallBackOptions();
    public void mainMenuCallBackHelp();
    public void mainMenuCallBackBookmark();

}
