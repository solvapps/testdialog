package solveraapps.testdialog;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import solveraapps.testdialog.dataclasses.IOptionHandler;
import solveraapps.testdialog.dataclasses.MapOptions;
import solveraapps.testdialog.dataclasses.OptionDialogPostProcess;
import solveraapps.testdialog.dataclasses.OptionHandler;
import solveraapps.testdialog.dataclasses.TimelineOptionDialog;
import solveraapps.testdialog.dialogs.MapOptionDialog;

public class MainActivity extends AppCompatActivity implements OptionDialogPostProcess {

    Button showMapOptionDialog;

    MapOptions mapOption;

    private Context context=this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final OptionHandler optionHandler = new OptionHandler();

        showMapOptionDialog = (Button) findViewById(R.id.btnOrder);
        showMapOptionDialog.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                LayoutInflater inflater = LayoutInflater.from(context);
                View mView;
                mView = inflater.inflate(R.layout.mainmenu, null);


                AlertDialog.Builder mBuilder = new AlertDialog.Builder(context, R.style.TransparentDialogStyle);
                mBuilder.setView(mView);
                final AlertDialog mDialog = mBuilder.create();
                mDialog.show();



//                LayoutInflater inflater = LayoutInflater.from(context);
//                View mView;
//                mView = inflater.inflate(R.layout.mainmenu,null);
//
//
//                AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
//                mBuilder.setView(mView);
//                final AlertDialog mDialog = mBuilder.create();
//                mDialog.show();



//                TimelineOptionDialog timelineOptionDialog = new TimelineOptionDialog(MainActivity.this,optionHandler);
//                timelineOptionDialog.showDialog();
//                MapOptionDialog mapOptionDialog = new MapOptionDialog(MainActivity.this,optionHandler);
//                mapOptionDialog.showDialog();







            }
        });
    }

    @Override
    public void optionDialogPostProcess() {

    }

    @Override
    public Context getContext() {
        return this;
    }
}
