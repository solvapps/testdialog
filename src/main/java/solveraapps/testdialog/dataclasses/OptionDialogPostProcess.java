package solveraapps.testdialog.dataclasses;

/**
 * Created by andreas on 05.12.2017.
 */

import android.content.Context;

/**
 * Alle Objekte, die dieses Interface implementieren koennen Optiondialoge verwenden und werden durch einen Callback aufgerufen.
 */
public interface OptionDialogPostProcess {

    public void optionDialogPostProcess();

    public Context getContext();




}
