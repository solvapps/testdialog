package solveraapps.testdialog.dataclasses;

import android.util.Log;

/**
 * Hier werden alle Chronica spezifische Optionen verwaltet. Die Daten werden per preferences gespeichert.
 * Das Interface ist da, damit man spaeter vielleicht die Daten in eine Cloud speichern kann.
 */

public class OptionHandler implements IOptionHandler {


    private MapOptions mapOptions = new MapOptions();

    @Override
    public MapOptions getMapOptions() {
        return mapOptions;
    }

    @Override
    public void saveMapOptions() {
        Log.v("DEBUG_O : mapOptions ",mapOptions.toString());
    }
}
